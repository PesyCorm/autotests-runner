from sys import platform
import socket
import subprocess


__all__ = ('check_os', 'check_docker', 'check_ports')


def check_os():
    return True if 'linux' in platform else False

def check_docker(): #!
    try:
        proc = subprocess.Popen('docker --version'.split(), stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        if proc.stdout.read():
            return True
    except:
        pass
    return False

def check_ports(ports):
    for port in ports:
        if not port.isdigit():
            return 'Порт(ы) должны быть в виде числа'
        if not 1025 <= int(port) <= 65535:
            return 'Указанный порт должен быть в диапозоне от 1025 до 65535'
    return check_ports_open_for_connection(ports)

def check_ports_open_for_connection(ports):
    closed_ports = []

    for port in ports:
        port = int(port)
        try:
            with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
                s.bind(('127.0.0.1', port))
        except socket.error:
            closed_ports.append(port)
    if closed_ports:
        return f'Порт(ы) {closed_ports} уже занят(ы)'