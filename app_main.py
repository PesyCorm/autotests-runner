import time

from flask import Flask, render_template, url_for, request, redirect, flash
from os import path, getcwd
from uuid import uuid4

from validators import *
from install_managers import *
from db_manager import *


app = Flask(__name__)
app.config['SECRET_KEY'] = uuid4().hex
db = ContainersDB()


ports_data = {
    'selenoid': '4444',
    'selenoid_ui': '8880',
    'jenkins': '8000'
}
framework_question = False


@app.route('/index')
@app.route('/')
def index():
    if path.exists(path.join(getcwd(), "data", "containers.db")):
        return render_template('index.html', title='Главная страница')
    else:
        return redirect(url_for('start_index'), 302)

@app.route('/start-index')
def start_index():
    return render_template('start_index.html', title='Давайте начнем')

@app.route('/explain')
def explain():
    return render_template('explain.html', os=check_os(), docker=check_docker(), title='Что будет')

@app.route('/config', methods=["GET", "POST"])
def config():
    # Слишком много всего в одном обработчике
    if request.method == "POST":
        global ports_data
        global framework_question

        form = dict(request.form)
        ports = {service: (port if port else ports_data[service]) for service, port in form.items()}

        err = check_ports(ports.values())

        if not err:
            framework_question = bool(form.get('download_framework'))
            ports_data = ports
            db.create_db()

            for name, port in ports_data.items():
                db.insert_values(name, port)
            return redirect(url_for('build'), 302)
        else:
            flash(err)
    return render_template('config.html', title='Проявите инициативу')

@app.route('/build')
def build():
    return render_template('build.html', title='Процесс необратим..')

@app.route('/build_inside')
def build_inside():
    overwrite_compose(ports_data)
    compose_build()
    if framework_question:
        download_framework()
    return 'ok'  # Нельзя возвращать bool или None из обработчика

@app.route('/about_project')
def about_project():
    return render_template('about_project.html', title='Что это')

@app.route('/about_me')
def about_me():
    return render_template('about_me.html', title='Об авторе')

@app.route('/how-simple')
def how_simple():
    return render_template('how_simple.html', title='Как просто')


if __name__ == "__main__":
    app.run(debug=True)