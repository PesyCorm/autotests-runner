import yaml
import subprocess


__all__ = ('overwrite_compose', 'compose_build', 'download_framework')


def overwrite_compose(ports: dict):
    with open('docker-compose/docker-compose.yml', 'r+') as dc:
        rdc = yaml.load(dc, Loader=yaml.FullLoader)
        for service, port in ports.items():
            rdc['services'][service]['ports'] = [f"{port}:{rdc['services'][service]['ports'][0].split(':')[1]}"]
        yaml.dump(rdc, dc)

def compose_build():
    proc = subprocess.Popen('docker-compose build docker-compose/ '
                            '1>./data/logs/compose-build-logs.log 2>./data/logs/compose-build-err.log',
                            shell=True)
    proc.wait()

def download_framework():
    proc = subprocess.Popen('pip install git+https://gitlab.com/PesyCorm/framework.git '
                            '1>./data/logs/framework-download-logs.log 2>./data/logs/framework-download-err.log',
                            shell=True)
    proc.wait()