import sqlite3
from os import getcwd, path
import json


__all__ = ('ContainersDB',)


class ContainersDB:

    def __init__(self):
        self.db_path = path.join(getcwd(), "data", "containers.db")

    def create_db(self):
        if not path.exists(self.db_path):
            self._create_db_file()
        self.db_conn = sqlite3.connect(self.db_path, check_same_thread=False)
        self._create_table()

    def _create_db_file(self):
        open(self.db_path, 'a').close()

    def _execute_sql(self, *sql):
        try:
            r = self.db_conn.execute(*sql)
            self.db_conn.commit()
            return r
        except Exception as err:
            print(f'Ошибка при работе с бд\n'
                  f'Скрипт, который пытались выполнить:\n{sql}\n'
                  f'Ошибка: {err}\n')

    def _create_table(self):
        """Создаем таблицу, если не существует"""
        script = """
                CREATE TABLE IF NOT EXISTS containers_info (
                name text NOT NULL unique,
                ports text
                )
                """
        self._execute_sql(script)

    def insert_values(self, name, port):
        """Записываем информацию о контейнере
        :param name: название контейнера
        :param port: порт
        """
        script = "INSERT INTO containers_info VALUES (?, ?)"
        self._execute_sql(script, (name, port))

    def get_container_info(self, name: str=None):
        """Получаем информацию о контейнере
        :param name: название
        или
        :param container_id: id
        :return: dict(name: str, container_id: str, ports: {порт внутри контейнера: внешний порт})
        """
        script = "SELECT * FROM containers_info WHERE name == ?"
        r = self._execute_sql(script, (name,))
        ddata = self._convert_data(r.fetchall())
        return ddata

    def _convert_data(self, data: tuple):
        ddata = dict(zip(('name', 'ports'), *data))
        ddata['ports'] = json.loads(ddata['ports'])
        return ddata